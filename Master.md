
## Java Operators
### | vs || and & vs &&
- The single operators (| and &) are bitwise operators.
    - Offer additional flexibility since they can work with integers and not only booleans.
- The double operators (|| and &&) are referred to as "Logical" or "Short-Circuit" operators. This is because they evaluate each expression in the line of operators until they find the first boolean result which asserts the overall result of the operator and then stop without evaluating the additional boolean expressions.
    - For the && operator, the first false encountered will guarantee an overall false response and terminate evaluation of the second boolean expression.
    - For the || operator, the first true encountered will guarantee an overall true response and terminate evaluation of the second boolean expression.
    - If there are multiple operators in a row (i.e. true || false && true) we need to look at operator precedence. In general, AND operators take precedence over OR operators and will be evaluated first. The formal docs are at: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
        - In the case of (true || false && true) the evaluation would follow this flow:
            - The second expression (in this case false) is the first to be evaluated. Since it is false and we are using a Short-Circuited AND operator, the third expression (true) will never be evaluated.
            - We will now look at the (true || false) expression where in this case, the false is the return from the first evaluation.
                - If the return of the first evaluation had been true, I do not know if that would short-circuit the OR operation.  
## Java Beans
Java Beans are POJOs with extra constraints.
- Private fields with getters and setters.
- Must include an explicit no-arg constructor.
- Must implement Serializable interface.
## Java Memory
### Stack vs Heap
- Primitive data types exist only within the stack and are never stored in heap.
- Object references are stored in the stack, but their instance variables are stored in the heap.
- As soon as a method ends, all local or method variables are removed from the stack, though if any reference variables are removed from the stack, the object that they reference may persist in heap.  
### Strings
There are two different types of String objects in Java. String literals are stored in the String Pool, a special subset of heap memory. String constructors are stored directly in heap and not in the String Pool.
- String pool String objects are reusable. You can have multiple String literals that reference the same String object in the String Pool.
- String objects stored in the heap directly will only be referred to by one reference variable. If we create a new String literal that has the same value as a String construct in the Heap memory, the String literal will not point to the String Constructor object in heap.  
## Keywords
- Final:
    - Class: Can not be extended.
    - Method: Can not be overridden.
    - Variable: Can not reassign.
        - A final reference variable can have its object's properties changed, but it cannot be assigned to a difference object.
        - A final primitive is a constant.
## Collections
- HashSets do not maintain order when adding items.
### HashMaps vs HashTables
- HashMaps are not thread safe, HashTables are thread safe.
- HashMaps are faster and generally preferred in all cases where you do not need to ensure thread safety.


### Comparators
**Comparable Interface:**  
- Must implement the compareTo() method.
- Should be specified with a generic type that it will compare:
    - implements Comparable\<T\>
        - The method created will have a signature with its input parameter typed as T
        - public static int compareTo(\<T\> o)

**Comparator Class:**  
- This is a custom class which implements Comparable.

## Exceptions and Errors
**General Heirarchy:**  
- Throwable
    - Errors: Stack Overflow, Out of Memory. Errors can not be recovered from and should end the program. There is nothing we can do about these.
    - Exceptions: We are responsible for these and can prevent or assign contingencies for. We should throw or handle exceptions.
        - Unchecked: These exceptions are not checked for by the compiler, but can occur during the runtime of the program.
            - In a production environment, we should try to ensure that these types of exceptions are not encountered. This will involve extensive testing. As such, these should not use a Try/Catch/Finally block.
        - Checked: These are exceptions that will prevent compilation unless a method of handling is implmented on some level.
            - In any environment, these cannot be predicted by the programmer as they generally involve outside resources, such as files that might throw an IOException, or a database that might throw an SQLException. We should write a Try/Catch/Finally block for all of these to provide a contigency in case this happens.
- Try/Catch/Finally Blocks work, but are slower and less desirable than simply preventing the exception in the first place. But they are still useful in some cases.  

**Throwing Exceptions to calling Class**  
Note that exceptions have to be handled at some stage.
```java
public void openFileForReading(String fileName) throws IOException {
    //Code that might throw an IOException
}
```
This code would pass the *IOException* to the calling class.

**Try/Catch/Finally Block**  
```java
public void openFileForReading(String fileName) {
    try {
        //Code that opens the file for reading.
        }
    catch (FileNotFoundException e) {
        e.printStackTrace();
    }
    catch (IOException e) {
        e.printStackTrace();
    }  
    finally {
        //Code that closes all opened resources.
        //This code will execute whether or not we get an exception
    }
}
```
Note that we can have multiple *Catch* blocks with different types of exceptions to be caught and different instructions. We can also list multiple exception types in a single *Catch* block using | seperator (IOException | ServletException). We should always order our *Catch* blocks from most specific to least specific. Additionally, we should avoid overly general exception catches i.e.  
```java
Catch (Exception e) {}
```
This is because once we have this exception catcher in our code, our IDE will no longer warn us or more specific exceptions our code might throw if we modify it in the future.  
The *Finally* block will **always** execute, whether or not an exception is thrown. This makes it great for cleanup work, like closing any resources that might have been opened in our *Try* block. This will ensure that no matter what happens with our code, the resources are closed.  
**A note about AutoClosable Resources**  
Some resources, such as *Scanner* are autoclosable and do not necessarily require a finally block to close them. Instead, you can use a *Try with resources*:
```java
Try (FileReader fr = new FileReader()) {}
```
All classes that implement the *AutoClosable* or *Closable* interfaces can be used in the *Try with resources* block and will be automatically closed if needed.  
**References:**  
- https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
- https://docs.oracle.com/javase/8/docs/api/java/io/Closeable.html
  

