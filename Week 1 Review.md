# Week 1 Review: March 1 - March 5, 2021  
## Monday, March 1  
### JDK, JRE, JVM  
**JVM (Java Virtual Machine)**
: A virtual machine that is run on the users device whenever they run a java program. It is this virtual machine that reads and run the java bytecode, rather than the users OS. Different OS's have different JVM implementations, but any java program can run on any appropriate JVM regardless of the users OS.  
**JRE (Java Runtime Environment)**
: The downloadable java environment that constitutes what most end users are likely to have on their system. This contains all required packages and classes for the JVM.  
**JDK (Java Development Kit)**
: A downloadable java environment that includes the JRE as well as a compiler, allowing developers to create and run their own java programs.  
### Class vs Object  
**Class**
: Java structure which can contain variables and methods and is often referred to as a blueprint, defining the structure of Objects. It should be noted, however, that not all classes are meant to be Instantiated into objects. Abstract Classes cannot be instantiated and many classes contain only static methods that can be called without instantiating an Object of that class.  
- A *Car* class might define variables such as *make* and *model* and methods such as *toggleRunning* and *drive()*, providing a template that all *Car* Objects will follow.
- The *Math* class in java defines the constants *e* and *pi* as well as methods such as *abs(int a)* and *ceil(double a)*, all of which are static and can be called without needing to instantiate a *Math* Object.  

**Object**
: A single instance of a Java Class. Objects have their own versions of all variables and methods defined in the class which are not marked as Static.
- A *Car* Object created using our *Car* class will have its own *make* and *model* variables and *toggleRunning()* and *drive()* methods that will be unique from other *Car* Objects.  

### Stack and Heap  
There are two main areas of memory used by java programs in different circumstances that are worked with in different ways.  
**Stack**
: The memory used during the runtime of a method containing all variables, primitive or reference required for the method to run. When a method finishes execution, its variables are removed from the stack. The stack is LIFO (last in first out) and only accessible within the scope of the current operation (If methodA calls methodB, methodB cannot access any of the variables that methodA has in the stack, though it can have its own version of any variables passed into it from methodA).  
**Heap**
: The longer term general memory used by java to hold all information that reference variables point to. In other words, it holds Object data. Unlike the stack, since this memory is not tied to a particular execution of a method, it is not automatically removed when execution finishes. Rather garbage collection will remove the data when it is no longer referenced (the Object it is tied to is deleted).  

### Garbage Collection  
This is an automatic process by which the JVM cleans its heap memory of any unreferenced objects. This requires no action on the part of the developer, though removing references to Objects that are no longer needed can help to ensure that that memory gets deallocated correctly. The JVM runs garbage collection on its own, but there are ways to request that it be run.
- https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html  
- https://www.geeksforgeeks.org/garbage-collection-java/  

### Operators
**Mathematical Operators**  
| Symbol | Meaning |
| ------ | ------- |
|   +    | Addition |
|   -    | Subtraction |
|   *    | Multiplication |
|   /    | Division |
|   %    | Modulus |  

**Logical Operators**  
| Symbol | Meaning |
| ------ | ------- |
|   &&   | AND (Short-Circuit) |
| \|\|   | OR (Short-Circuit) |
|   &    | Bitwise AND |
|   \|   | Bitwise Inclusive OR |  
|   ==   | Equal |
|   !=   | Not Equal |
|   !    | Logical Not (negation) |
|   >    | Greater Than |
|   >=   | Greater Than or Equal To |
|   <    | Less Than |
|   <=   | Less Than or Equal To |  

**Assignment Operators**
| Symbol | Meaning |
| ------ | ------- |
|    =   | Assignment |
|   +=   | Add and Reassign |
|   -=   | Subtract and Reassign |
|   *=   | Multiply and Reassign |
|   /=   | Divide and Reassign |
|   %=   | Modulo and Reassign |  

**Unary Operators**  
| Symbol | Meaning |
| ------ | ------- |
|   ++   | Increment |
|   --   | Decrement |  

\* Note that these can be both postfix and prefix operators.  

Other operators exist, such as bitwise and shift operators that we really haven't covered much.  
\>>, >>>, <<, <<=, >>>=, ^=, |=, &=, ^

- https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html 
- https://www.w3schools.com/java/java_operators.asp  


### Control Flow Statements
**IF/Else**  
Simple logical control flow statements. If statements require a boolean expression, but that expression can include other expressions and logical operators. If the if statement's expression evaluates to false, execution skips the contents of the if block (which does not need to be enclosed in curly braces {} if it is only a single line). Else statements execute only if all preceding if statements evaluate to false. An else can be followed by another if, maintaining the if sequence.
```java
int x = 5;
if (x % 3 == 0 && x % 5 ==0)
    System.out.println("FizzBuzz");
else if (x % 5 == 0)
    System.out.println("Buzz");
else if (x % 3 ==0)
    System.out.println("Fizz");
else
    System.out.println(x);
```  
**Switch**
Switch statements are used to compare the result of an evaluation to a number of different cases. Important things to note with switch statements:
- Switch execution blocks start with the first matching case and will continue until the end of the switch statement or the first `break;` encountered.
- Switch statements can be ended with a `default:` expression block which will execute if all other cases fail.
- A `break;` encountered anywhere in the switch statement will exit the statement completely and subsequent cases, including `default:` will be tested. 
- Switch expression blocks are not enclosed in curly braces {}  

The following block was copied from TutorialsPoint at:
https://www.tutorialspoint.com/java/switch_statement_in_java.htm#:~:text=Advertisements,is%20checked%20for%20each%20case.  

```java
char grade = 'C';

    switch(grade) {
        case 'A' :
            System.out.println("Excellent!"); 
            break;
        case 'B' :
        case 'C' :
            System.out.println("Well done");
            break;
        case 'D' :
            System.out.println("You passed");
        case 'F' :
            System.out.println("Better try again");
            break;
        default :
            System.out.println("Invalid grade");
    }
```

### Variable Scope
Variables exist within a certain scope. Within this scope that can be accessed and possibly altered, but outside of this scope they are invisible. For example, variables declared within a method are in the methods scope and cannot be seen by other methods, while instance variables tied to an Object only exist within the scope of that Object.   
This means that if we define an instance variable `doors` for our `Car` class, then instantiate two instances of `Car` Objects. Each will have its own `doors` variable which share a name, but can have different values and are accessed differently (`car1.doors` and `car2.doors`). This also means that if we have two methods in our class which we use to add two numbers, for example one that adds int types and one that adds float types, both methods can declare and return a variable `sum` without conflict.

### Static Keyword  
Static can be used to define properties and methods of a class, indicating that the variable or method belongs to the class itself, and not to any instantiated Object instance of the class.
- Static methods can be called directly from the method without instantiating an Object. For example, we can call the `Math.abs()` method without having to create a `Math` Object first.
- Static variables have a single value which is tied to the class, and not to any Objects. The value of this variable may be changed as long as it is not final, but if it is changed with one Object, it changes for all Objects. Also, like a method, an Object does not have to be created to access a static variable. For example, we can use `Math.PI` to reference the static variable containing the value of *pi* without instantiating a `Math` Object.

### Access Modifiers  
Access modifiers are used to define access different entities have to our classes, variables or methods.
**Default**  
- Used if no other access modifier is specified.
- Can be used with classes, variables and methods
- Entities with the default access modifier applied are accessible by all other entities in the same package, but not by any entity outside of the package.
    - A class with default access cannot have any of its variables or methods accessed by anything outside of its own package, even if those methods or variables are made public.
    http://tutorials.jenkov.com/java/access-modifiers.html#class-access-modifiers  

**Public**  
- Can be used with classes, variables and methods.
- Accessible throughout the program with no restrictions.  

**Protected**  
- Can be used with variables and methods only.
- Protected entities are accessible within the same package, or by subclasses in other packages.

**Private**  
- Can be used with variables and methods only.
- Private entities are only available to that class.  

### Packages and Imports
Packages are logical groupings of classes (including interrfaces and enums) which are similar or used together commonly. A class in one package does not need to import any other classes in the same package to have access to them (or at least as much access as is granted by access modifiers).  
Classes in other packages will need to be imported to be accessed. Importing a class from another package does not allow you to treat it as if it is in the same package (for purposes of default or protected access modifiers), but rather directs your class on where to find it. Classes are imported with their full path package specified.
```java
import com.kkanit.models.Car;
import com.kkanit.servlets.CarManagerServlet;
import java.util.*;
```

### Constructors
Constructors are methods used to instantiate an Object of a given class. Java will give you a basic no-arg constructor as long as you do not declare any of your own. Constructors have no explicit return, and while commonly public, can have any access modifier. If you declare any constructor with arguments, you should also always declare a no-arg constructor if your class can be extended since any subclass constructor will implicitly call its super classes no-arg constructor.
```java
public Class Car {

    private int doors;

    public Car() {}

    public Car(int doors) {
        this.doors = doors;
    }
}
```  

### Methods and Parameters
Methods are functions belonging to an Object or class. Methods may or may not have a single return type. Methods may or may not take parameters which will act as locally scoped variables in the execution of the method. Method parameters must be declared by type and assigned a valid variable name.
```java
public static void main(String[] args) {
    // Accepts a single parameter in the form of an array containing String Objects which is assigned to the variable args.
    // Has no return.
}
private int add(int num1, int num2) {
    // Accepts two parameters, each an int assigned to num1 and num2 respectively.
    // Returns a single int value and MUST do so to avoid compile errors.
}
```

### GIT
Git can be a complicated topic with multiple components and scopes. We will start by discussing a local git repository which includes untracked files, tracked files, staged files and commits.  
**git init**  
: Used to initialize a new git repository in the local working directory. 
**.gitignore**  
: A special file in your main git working directory which includes a list of files and file types to be ignored (untracked) by git.  
**git add .**  
: Used to add all tracked files in your git working directory to the staging area (i.e. telling git that you wish to commit changes to these files when you perform the next git commit operation). Not all tracked files have to be staged for each commit, but any unstaged files will not have their changes included in the commit. We do not have   
**git commit -m "message"**  
: Used to commit all changes in any staged files to a new git commit version with the message after the -m. A message must be included for all git commits, and if we omit the -m "message" portion of the command, a text editor will open requiring a valid message before the staged changes can be committed.








